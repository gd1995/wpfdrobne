﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    public class Osoba : INotifyPropertyChanged
    {
        private string name, surname, mail, region, userString;
        private double money;
        private int access;
        Boolean details;
        public string Name { get { return name; } set { name = value; OnPropertyChanged("userString"); } }
        public string Surname { get { return surname; } set { surname = value; OnPropertyChanged("userString"); } }
        public string Mail { get { return mail; } set { mail = value; OnPropertyChanged("userString"); } }
        public string Region { get { return region; } set { region = value; } }
        public double Money { get { return money; } set { money = value; } }
        public int Access { get { return access; } set { access = value; } }
        public Boolean Details { get { return details; } set { details = value; } }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public Osoba()
        {
            details = false;
        }
        public Osoba(string name, string surname)
        {
            this.name = name;
            this.surname = surname;
            details = false;
        }
        public override string ToString()
        {
            string toString = name + " " + surname;
            if (mail != null && !mail.Equals(string.Empty))
            {
                toString += " (" + mail + ")";
            }
            return toString;
        }

        public string UserString
        {
            get
            {
                if (details == true)
                {
                    return name + " " + surname + " (" + mail + ")";
                }
                else
                    return name + " " + surname;
            }
            set
            {
                userString = value;
            }
        }

    }
}
