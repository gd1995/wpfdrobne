﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string> region = new List<string>();
        public List<string> Region { get { return region; } }
        //List<Person> people = new List<Person>();
        ObservableCollection<Osoba> people = new ObservableCollection<Osoba>();

        public ObservableCollection<Osoba> People { get { return people; } }
        public MainWindow()
        {
            InitializeComponent();
            region.Add("Białystok");
            region.Add("Kraków");
            region.Add("Warszawa");
            cbRegion.ItemsSource = Region;
            Osoba p = new Osoba("Dodaj nową", "osobę...");
            people.Add(p);
            lbOsoba.ItemsSource = People;
        }
        private void buttonDelete_Click(object sender, RoutedEventArgs e)
        {
            people.RemoveAt(lbOsoba.SelectedIndex);
        }
        private void lbOsoba_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbOsoba.SelectedIndex == people.Count - 1)
            {
                Osoba p = new Osoba("Nowy", "Obiekt");
                int index = people.Count - 1;
                people.Insert(index, p);
                lbOsoba.SelectedIndex = index;
                tbImie.SelectAll();
                tbImie.Focus();
            }
        }
        private void TextBoxGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            box.SelectAll();
            box.Focus();
        }
    }
}
